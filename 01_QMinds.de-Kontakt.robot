*** Settings ***
Documentation     Test Case 1
Force Tags        Required
Library           RobotEyes
Library           SeleniumLibrary
Resource          Library/Resource.robot
Resource          Library/00_QMinds.de-Common.robot
Resource          Library/00_QMinds.de-CommonUI.robot
Resource          Library/01_QMinds.de-Kontakt.robot
Resource          Library/01_QMinds.de-KontaktUI.robot

*** Test Cases ***
01_QMinds.de-Kontakt
    [Documentation]    Test case 1:
    ...    Preconditions:
    ...    0-1 Go to www.qualityminds.de -> URL QualityMinds page is opened
    ...    0-2 Check if CookieMsg is visable and accept Cookies -> CookieMsg is not visable
    ...    *Steps Expected -> Results*
    ...    2 Click on “Kontakt” at the top navigation bar of the page -> Kontakt & Anfahrt page is displayed
    ...    3 Verify if the page contains email address: hello@qualityminds.de -> Page contains hello@qualityminds.de email address
    ...    4 Navigate back to www.qualityminds.de main page -> QualityMinds page is displayed
    ...    5 Click on KONTAKT & ANFAHRT link at the bottom of the page -> Kontakt & Anfahrt page is displayed
    ...    6 Verify if the page displayed in step 2 is the same page that is displayed in step 5 -> Pages displayed in step 2 and 5 are the same
    Step 2: Click top Menu Kontakt and Check header and URL
    Step 2a: Open Eyes and Capture Screen for Visual Comaprison
    Step 3: Verify hello@qualityminds.de exist on website
    Step 4: Navigate back to main page and Check URL
    Step 5: Click footer Kontakt and Check header and URL
    Step 5a: Capture Screen with Eyes for Visual Comaprison
    Step 6: Visually Compare with Baseline images from step 2a and 5a

*** Keywords ***
