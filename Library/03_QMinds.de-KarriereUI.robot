*** Variables ***
${Karriere_headerText XPath}    //h1[.='Werde ein QualityMind!']
${Karriere_Apply-Now Button no 1 XPath}    //a[.='Bewirb dich jetzt!'][contains(@class,'et_pb_button et_pb_button_0')]
${Karriere_Apply-Now Button no 2 XPath}    //a[.='Bewirb dich jetzt!'][contains(@class,'et_pb_button et_pb_button_1')]
${Karriere_Contact Form URL}    kontaktformular/
${Karriere_ContactForm_First name Input required XPath}    //input[@placeholder='Vorname*'][@required]
${Karriere_ContactForm_Surname Input required XPath}    //input[@placeholder='Nachname*'][@required]
${Karriere_ContactForm_Email Input required XPath}    //input[@placeholder='Email*'][@required]
${Karriere_ContactForm_Field Validation Message XPath suffix}    /../../span[.='Dies ist ein Pflichtfeld.']
${Karriere_ContactForm_Invalid Email Validation Message XPath suffix}    /../../span[.='Die Eingabe muss eine gültige E-Mail-Adresse sein.']
${Karriere_ContactForm_Submit form Button XPath}    //input[@value='Jetzt Bewerben'][contains(@class, 'qm-button')][@type='submit']
${Karriere_ContactForm_Upload file Button XPath}    //button[.='Dateien hochladen']
${Karriere_ContactForm_Uploaded file Name XPath}    //button[.='Dateien hochladen']/../..//span[.='QM_Testing.jpg']
${Karriere_ContactForm_Upload file Input XPath}    //button[.='Dateien hochladen']//..//input[@type='file']
${Karriere_ContactForm_Data protection Checkbox required XPath}    //label[contains(text(),'Datenschutzerklärung')]/../div/label/input[@type='checkbox'][@data-parsley-required='true']
${Karriere_ContactForm_Uploaded files Names TEMPLATE XPath}    //button[.='Dateien hochladen']/../..//span[.='__file_name__']
${Karriere_ContactForm_Message after Submit Text}    Vielen Dank für deine Bewerbung bei QualityMinds!
