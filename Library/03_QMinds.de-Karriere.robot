*** Settings ***
Library           SeleniumLibrary
Library           Collections
Library           FakerLibrary    locale=de_DE
Library           String
Resource          Resource.robot
Resource          00_QMinds.de-Common.robot
Resource          00_QMinds.de-CommonUI.robot
Resource          03_QMinds.de-KarriereUI.robot

*** Keywords ***
Step 1: Click top Menu Karriere and Check header
    Focus and Click element    ${TopMenu_Karriere XPath}
    Wait Until Element Is Visible    ${Karriere_headerText XPath}

Step 2: Click Apply now button and Check form
    ${are_Elements_Atributes_Equal}    Compare attributes of 2 elements    ${Karriere_Apply-Now Button no 1 XPath}    href    ${Karriere_Apply-Now Button no 2 XPath}    href
    Run Keyword If    '${are_Elements_Atributes_Equal}'=='True'    Focus and Click element    ${Karriere_Apply-Now Button no 1 XPath}
    ...    ELSE    Fail    Href of 'Apply now!' buttons are not equal! There is a bug or code of test should be changed.
    Location Should Be    ${HOMEPAGE}[${ENV}]${Karriere_Contact Form URL}

Step 3: Click form Submit button and check if it was not submited
    Focus and Click element    ${Karriere_ContactForm_Submit form Button XPath}
    Wait Until Element Is Enabled    ${Karriere_ContactForm_Submit form Button XPath}
    Page Should Not Contain    ${Karriere_ContactForm_Message after Submit Text}

Step 4: Verify validation messages for 3 fields
    @{Fields2test List}    Create List    ${Karriere_ContactForm_First name Input required XPath}${Karriere_ContactForm_Field Validation Message XPath suffix}    ${Karriere_ContactForm_Surname Input required XPath}${Karriere_ContactForm_Field Validation Message XPath suffix}    ${Karriere_ContactForm_Email Input required XPath}${Karriere_ContactForm_Field Validation Message XPath suffix}
    Scroll and check elements    @{Fields2test List}

Step 5: Fill and check First name and Surname fields
    @{Test_data}    Generate test data faker    td_first_name=Yes    td_surname=Yes
    Fill input with text    ${Karriere_ContactForm_First name Input required XPath}    ${Test_data}[0]
    Fill input with text    ${Karriere_ContactForm_Surname Input required XPath}    ${Test_data}[1]

Step 6: Click form Submit button and check if it was not submited
    Step 3: Click form Submit button and check if it was not submited

Step 7: Verify email field validation message
    Scroll and check element    ${Karriere_ContactForm_Email Input required XPath}${Karriere_ContactForm_Field Validation Message XPath suffix}

Step 8-9: Fill invalid emails Submit and check validation message
    Fill input with invalid data, click and check message    ${Karriere_ContactForm_Email Input required XPath}    ${Invalid_Emails}[0:2]    ${Karriere_ContactForm_Submit form Button XPath}    ${Karriere_ContactForm_Email Input required XPath}${Karriere_ContactForm_Invalid Email Validation Message XPath suffix}

Step 10: Attach file and check it
    Choose File    ${Karriere_ContactForm_Upload file Input XPath}    ${Test_file Path}
    Scroll and check element    ${Karriere_ContactForm_Uploaded file Name XPath}

Step 10a: Attach 4 files and check it
    FOR    ${Test_file Name}    IN    @{Test_file Names}
        Choose File    ${Karriere_ContactForm_Upload file Input XPath}    ${Test_file Catalog Path}${Test_file Name}
        ${Karriere_ContactForm_Uploaded files Names XPath}    Replace String    ${Karriere_ContactForm_Uploaded files Names TEMPLATE XPath}    __file_name__    ${Test_file Name}
        Scroll and check element    ${Karriere_ContactForm_Uploaded file Name XPath}
    END

Step 11: Check Data protection checkbox
    Select checkbox if unchecked    ${Karriere_ContactForm_Data protection Checkbox required XPath}
