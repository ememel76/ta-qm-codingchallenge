*** Settings ***
Library           SeleniumLibrary
Library           OperatingSystem
Library           00_CommonElements/CustomLibraries/DownloadFile.py

*** Keywords ***
Go to HomePage
    Set Selenium Speed    ${DELAY}
    Open Browser    ${HOMEPAGE}[${ENV}]    ${BROWSER}
    Maximize Browser Window
    Location Should Be    ${HOMEPAGE}[${ENV}]

Check CookieMsg
    ${isCookieMsgVisible}    Run Keyword And Return Status    Wait Until Element Is Visible    ${CookieMsg XPath}    5s
    Run Keyword If    ${isCookieMsgVisible}    Run Keywords
    Click Element    ${CookiesAllow Button XPath}
    Run Keyword And Return Status    Element Should Not Be Visible    ${CookieMsg XPath}

Back to HomePage
    Go To    ${HOMEPAGE}[${ENV}]
    Location Should Be    ${HOMEPAGE}[${ENV}]

Focus and Click element
    [Arguments]    ${Element XPath}
    Wait Until Element Is Visible    ${Element XPath}    10s
    Set Focus To Element    ${Element XPath}
    Click Element    ${Element XPath}

Compare attributes of 2 elements
    [Arguments]    ${Element 1 XPath}    ${Element 1 Attribute}    ${Element 2 XPath}    ${Element 2 Attribute}
    ${Element 1 Attribute Value}    Get Element Attribute    ${Element 1 XPath}    ${Element 1 Attribute}
    ${Element 2 Attribute Value}    Get Element Attribute    ${Element 2 XPath}    ${Element 2 Attribute}
    ${are_Elements_Atributes_Equal}    Set Variable If    '${Element 1 Attribute Value}'=='${Element 2 Attribute Value}'    True    False
    [Teardown]
    [Return]    ${are_Elements_Atributes_Equal}

Scroll and check element
    [Arguments]    ${Element}
    Scroll Element Into View    ${Element}
    Page Should Contain Element    ${Element}

Scroll and check elements
    [Arguments]    @{Elements List}
    ${Length}    Get Length    ${Elements List}
    FOR    ${i}    IN RANGE    ${Length}
        Scroll and check element    ${Elements List}[${i}]
    END

Generate test data faker
    [Arguments]    ${td_first_name}=No    ${td_surname}=No    ${td_email}=No
    @{Test_Data}    Create List
    ${first_name}    Run Keyword If    '${td_first_name}'=='Yes'    FakerLibrary.First Name
    Run Keyword If    '${td_first_name}'=='Yes'    Append To List    ${Test_Data}    ${first_name}
    ${surname}    Run Keyword If    '${td_surname}'=='Yes'    FakerLibrary.Last Name
    Run Keyword If    '${td_surname}'=='Yes'    Append To List    ${Test_Data}    ${surname}
    ${email}    Run Keyword If    '${td_email}'=='Yes'    FakerLibrary.Email
    Run Keyword If    '${td_email}'=='Yes'    Append To List    ${Test_Data}    ${email}
    [Return]    @{Test_Data}

Fill input with text
    [Arguments]    ${Element XPath}    ${Text}
    Wait Until Element Is Enabled    ${Element XPath}
    Set Focus To Element    ${Element XPath}
    Input Text    ${Element XPath}    ${Text}

Select checkbox if unchecked
    [Arguments]    ${Checkbox XPath}
    ${Is_Checkbox_Selected}=    Run Keyword And Return Status    Checkbox Should Be Selected    ${Checkbox XPath}
    Run Keyword If    '${Is_Checkbox_Selected}'=='False'    Click Element    ${Checkbox XPath}
    Checkbox Should Be Selected    ${Checkbox XPath}

Setup DO NOTHING On Failures
    Register Keyword To Run On Failure    NOTHING

Teardown DO SCREENSHOT On Test Failure
    Run Keyword If Test Failed    Capture Page Screenshot

Create temp Download Folder
    ${now}    Get Time    epoch
    ${temp_DownloadFolder}    Join Path    ${EXECDIR}${/}Library${/}temp_Download${/}    downloads_${now}
    Set Global Variable    ${temp_DownloadFolder}
    Create Directory    ${temp_DownloadFolder}

Delete Folder
    [Arguments]    ${folder}
    Remove Directory    ${folder}    recursive=True
    Directory Should Not Exist    ${folder}

Download file from locator
    [Arguments]    ${Locator}    ${File_path_name}
    ${File_URL}    Get Element Attribute    ${Locator}    href
    Download File    ${File_URL}    ${File_path_name}
    Comment    ${Downloaded_File_Path}    Set Variable    ${temp_DownloadFolder}${/}${File_name}

Check if file size is greater then 0
    [Arguments]    ${File_path_name}
    ${File_size} =    Get File Size    ${File_path_name}
    Should Be True    ${File_size}>0

Get CSS Property Value
    [Arguments]    ${Locator}    ${Attribute_name}
    [Documentation]    Get the CSS property value of an Element.
    ...    This keyword retrieves the CSS property value of an element. The element is retrieved using the locator.
    ...    Arguments:
    ...    - locator (string) any Selenium Library supported locator xpath/css/id etc.
    ...    - property_name (string) the name of the css property for which the value is returned.
    ...    Returns (string) returns the string value of the given css attribute or fails.
    ${css}=    Get WebElement    ${Locator}
    ${Property_value}=    Call Method    ${css}    value_of_css_property    ${Attribute_name}
    [Return]    ${Property_value}

Fill input with invalid data, click and check message
    [Arguments]    ${Input XPath}    ${Text list}    ${Element2Click XPath}    ${Element2Check XPath}
    FOR    ${Invalid_Email}    IN    @{Text list}
        Fill input with text    ${Input XPath}    ${Invalid_Email}
        Focus and Click element    ${Element2Click XPath}
        Scroll and check element    ${Element2Check XPath}
    END
