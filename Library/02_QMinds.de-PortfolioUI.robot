*** Variables ***
${Portfolio_Testing WAM Testing_headerText XPath}    //h1[.='Web, Automation & Mobile Testing']
${Portfolio_Testing WAM Testing URL}    team_page/wam-testing/
${Portfolio_Testing WAM Clearfix-desktop Mobile XPath}    //div[contains(@id,'desktop') and contains(@class,'clearfix')]/div[.='Mobile']
${Portfolio_Testing WAM Clearfix-mobile Mobile XPath}    //div[contains(@id,'mobile') and contains(@class,'clearfix')]/div[.='Mobile']
${Portfolio_Testing WAM Download Flyer FtBS XPath}    //a[@download='FLYER FIND THE BUG SESSION']
${Portfolio_Testing WAM Download Flyer FtBS PDF href}    app/uploads/2018/11/Find-The-Mobile-Bug-Session.pdf
${Portfolio_Testing WAM Mobile section XPath}    //div[@id='team-tab-three-body']    # and not(contains(@style,'none'))
${Portfolio_Testing WAM Mobile4Desktop active-team-tab XPath}    //*[contains(@class,' active-team-tab')]/div/div[.='Mobile']/../..
&{Reference CSS_Mobile_border-bottom-width value}    Firefox=2.4px    Chrome=3px
&{Reference CSS_Mobile_border-bottom-color value}    Firefox=rgb(151, 151, 151)    Chrome=rgba(151, 151, 151, 1)
