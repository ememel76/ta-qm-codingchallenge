*** Variables ***
${CookieMsg XPath}    //span[contains(@id,"cookieconsent:")]
${CookiesAllow Button XPath}    //a[@aria-label='allow cookies'][@role='button']
${TopMenu_Kontakt XPath}    //ul[@id='top-menu']/li/a[.='Kontakt']
${FooterMenu_Kontakt XPath}    //ul[@id='menu-footer-menu']/li/a[.='Kontakt & Anfahrt']
${Kontakt URL}    kontakt/
&{CaptureScreenFileName Browser}    Firefox=ff_    Chrome=gc_
${TopMenu_Portfolio XPath}    //ul[@id='top-menu']/li/a[.='Portfolio']
${SubMenu_Portfolio HoverOn XPath}    //ul[@class='sub-menu']/../../li[contains(@class,'et-show-dropdown et-hover')]/a[.='Portfolio']
${SubMenu_Portfolio Highlighted XPath}    //ul[@class='sub-menu']/../../li[contains(@class,'current-menu-ancestor')]/a[.='Portfolio']
${SubMenu_Portfolio_Testing WAM Testing XPath}    //ul[@class='sub-menu']//a[.='Web, Automation & Mobile Testing']
${TopMenu_Karriere XPath}    //ul[@id='top-menu']/li/a[.='Karriere']
@{Invalid_Emails}    plainaddress    @%^%#$@#$@#.com    @example.com    Joe Smith <email@example.com>    email.example.com    email@example@example.com    .email@example.com    email.@example.com    email..email@example.com    email@example.com (Joe Smith)    email@example    email@-example.com    email@111.222.333.44444    email@example..com    Abc..123@example.com    # valid emails: סבבה@elpmaxe.moc | あいうえお@example.com
${Test_file Path}    ${EXECDIR}${/}Library${/}00_CommonElements${/}File2Test${/}QM_Testing.jpg
${Test_file Catalog Path}    ${EXECDIR}${/}Library${/}00_CommonElements${/}File2Test${/}
@{Test_file Names}    invalid_file123B.pdf    invalid_file6MB.pdf    valid_file_123B.doc    valid_file_6MB.doc
