*** Settings ***
Library           SeleniumLibrary
Library           OperatingSystem
Resource          Resource.robot
Resource          00_QMinds.de-Common.robot
Resource          00_QMinds.de-CommonUI.robot
Resource          02_QMinds.de-PortfolioUI.robot

*** Keywords ***
Step 2: Hover on Portfolio bar and Check submenu
    Mouse Over    ${TopMenu_Portfolio XPath}
    Element Should Be Visible    ${SubMenu_Portfolio HoverOn XPath}

Step 3: Click WAM Testing sub option and Check header and URL
    Focus and Click element    ${SubMenu_Portfolio_Testing WAM Testing XPath}
    Wait Until Element Is Visible    ${Portfolio_Testing WAM Testing_headerText XPath}    5s
    Location Should Be    ${HOMEPAGE}[${ENV}]${Portfolio_Testing WAM Testing URL}

Step 4: Verify that the Portfolio item is highlighted
    Element Should Be Visible    ${SubMenu_Portfolio Highlighted XPath}

Step 5: Click Mobile tab and Check section
    Focus and Click element    ${Portfolio_Testing WAM Clearfix-desktop Mobile XPath}
    Wait Until Element Is Visible    ${Portfolio_Testing WAM Mobile section XPath}    5s

Step 5a: Check if Mobile section tab is underlined and grey
    ${CSS_Mobile_border-bottom-width value} =    Get CSS Property Value    ${Portfolio_Testing WAM Mobile4Desktop active-team-tab XPath}    border-bottom-width
    Should Be Equal As Strings    ${CSS_Mobile_border-bottom-width value}    ${Reference CSS_Mobile_border-bottom-width value}[${Browser}]
    ${CSS_Mobile_border-bottom-color value} =    Get CSS Property Value    ${Portfolio_Testing WAM Mobile4Desktop active-team-tab XPath}    border-bottom-color
    Should Be Equal As Strings    ${CSS_Mobile_border-bottom-color value}    ${Reference CSS_Mobile_border-bottom-color value}[${Browser}]

Step 5b: Check if Flyer button is displayed on the right
    Element Should Be Visible    ${Portfolio_Testing WAM Download Flyer FtBS XPath}
    ${element_x_coordinate}    Get Horizontal Position    ${Portfolio_Testing WAM Download Flyer FtBS XPath}
    ${window_width}    ${window_height}    Get Window Size
    Should Be True    ${element_x_coordinate}>${window_width}/2

Step 6: Verify if Flyer link is what expected
    ${Portfolio_Testing WAM Flyer FtBS href attribute URL}    Get Element Attribute    ${Portfolio_Testing WAM Download Flyer FtBS XPath}    href
    Should Be Equal    ${Portfolio_Testing WAM Flyer FtBS href attribute URL}    ${HOMEPAGE}[${ENV}]${Portfolio_Testing WAM Download Flyer FtBS PDF href}

Step 7: Download Flyer and Check if it has size
    Download File    ${HOMEPAGE}[${ENV}]${Portfolio_Testing WAM Download Flyer FtBS PDF href}    ${temp_DownloadFolder}${/}test_Find-The-Mobile-Bug-Session.pdf
    Check if file size is greater then 0    ${temp_DownloadFolder}${/}test_Find-The-Mobile-Bug-Session.pdf
