*** Settings ***
Library           RobotEyes
Library           SeleniumLibrary
Resource          00_QMinds.de-Common.robot
Resource          00_QMinds.de-CommonUI.robot
Resource          01_QMinds.de-KontaktUI.robot

*** Keywords ***
Step 2: Click top Menu Kontakt and Check header and URL
    Focus and Click element    ${TopMenu_Kontakt XPath}
    Wait Until Element Is Visible    ${Kontakt_headerText XPath}    5s
    ${TopMenu_Kontakt URL_Get Location}    Get Location
    Should Be Equal    ${TopMenu_Kontakt URL_Get Location}    ${HOMEPAGE}[${ENV}]${Kontakt URL}

Step 2a: Open Eyes and Capture Screen for Visual Comaprison
    Open Eyes    SeleniumLibrary    tolerance=10
    Capture Full Screen    name=${CaptureScreenFileName Browser}[${Browser}]step2

Step 3: Verify hello@qualityminds.de exist on website
    Page Should Contain    hello@qualityminds.de

Step 4: Navigate back to main page and Check URL
    Back to HomePage

Step 5: Click footer Kontakt and Check header and URL
    Focus and Click element    ${FooterMenu_Kontakt XPath}
    Wait Until Element Is Visible    ${Kontakt_headerText XPath}    20s
    ${FooterMenu_Kontakt URL_Get Location}    Get Location
    Should Be Equal    ${FooterMenu_Kontakt URL_Get Location}    ${HOMEPAGE}[${ENV}]${Kontakt URL}

Step 5a: Capture Screen with Eyes for Visual Comaprison
    Capture Full Screen    name=${CaptureScreenFileName Browser}[${Browser}]step6

Step 6: Visually Compare with Baseline images from step 2a and 5a
    Compare Images
