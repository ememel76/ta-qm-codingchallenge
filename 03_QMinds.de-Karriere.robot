*** Settings ***
Documentation     Test Case 3
Force Tags        Required
Library           SeleniumLibrary
Library           Collections
Library           FakerLibrary    locale=de_DE
Library           String
Resource          Library/Resource.robot
Resource          Library/00_QMinds.de-Common.robot
Resource          Library/00_QMinds.de-CommonUI.robot
Resource          Library/03_QMinds.de-Karriere.robot
Resource          Library/03_QMinds.de-KarriereUI.robot

*** Test Cases ***
03_QMinds.de-Karriere
    [Documentation]    Test case 3:
    ...    Preconditions:
    ...    0-1 Go to www.qualityminds.de -> URL QualityMinds page is opened
    ...    0-2 Check if CookieMsg is visable and accept Cookies -> CookieMsg is not visable
    ...    *Steps Expected -> Results*
    ...    1 Click on the Karriere link in top navigation bar -> A Karriere page is opened
    ...    2 Click on Bewirb Dich Jetzt! button -> An application form is opened
    ...    3 Click on Jetzt bewerben button -> Message is not be submitted
    ...    4 Verify if validation messages are displayed -> Validation messages were displayed for 3 fields
    ...    5 Fill the Vorname and Nachname field -> Fields are filled
    ...    6 Click on Jetzt bewerben button -> Message is not submitted
    ...    7 Verify if validation messages are displayed -> Validation messages were displayed for email field
    ...    8 Fill the E-mail field with an Invalid value -> Email field is filled
    ...    9 Click on Jetzt bewerben button -> Message is not submitted
    ...    10 Attach file with DATEIEN HOCHLADEN button -> Filename of attachment is displayed above DATEIEN HOCHLADEN button
    ...    11 Check the checkbox for Datenschutzerklärung -> Checkbox is checked
    [Setup]    Setup DO NOTHING On Failures
    Step 1: Click top Menu Karriere and Check header
    Step 2: Click Apply now button and Check form
    Step 3: Click form Submit button and check if it was not submited
    Step 4: Verify validation messages for 3 fields
    Step 5: Fill and check First name and Surname fields
    Step 6: Click form Submit button and check if it was not submited
    Step 7: Verify email field validation message
    Step 8-9: Fill invalid emails Submit and check validation message
    Step 10: Attach file and check it
    Step 10a: Attach 4 files and check it    # 10a Attach 4 files (2 of them larger then 5MB) with DATEIEN HOCHLADEN button -> Filenames of attachments are displayed above DATEIEN HOCHLADEN button
    Step 11: Check Data protection checkbox
    [Teardown]    Teardown DO SCREENSHOT On Test Failure

*** Keywords ***
