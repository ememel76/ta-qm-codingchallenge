*** Settings ***
Documentation     Test Case 2
Force Tags        Required
Library           SeleniumLibrary
Library           OperatingSystem
Resource          Library/Resource.robot
Resource          Library/00_QMinds.de-Common.robot
Resource          Library/00_QMinds.de-CommonUI.robot
Resource          Library/02_QMinds.de-Portfolio.robot
Resource          Library/02_QMinds.de-PortfolioUI.robot

*** Test Cases ***
02_QMinds.de-Portfolio
    [Documentation]    Test case 2:
    ...    Preconditions:
    ...    0-1 Go to www.qualityminds.de -> URL QualityMinds page is opened
    ...    0-2 Check if CookieMsg is visable and accept Cookies -> CookieMsg is not visable
    ...    *Steps Expected -> Results*
    ...    2 Hover on Portfolio at the top navigation bar of the page and verify if submenu is displayed -> Submenu is displayed
    ...    3 Click on Web, Automation & Mobile Testing sub option -> Web, Automation & Mobile Testing page is displayed
    ...    4 Verify that the Portfolio item of the top bar menu is highlighted -> Portfolio item of the top bar menu is highlighted
    ...    5 Click on Mobile tab in “Web – Automatisierung – Mobile” section -> Mobile section content is displayed, “Mobile” is underlined in grey, “Flyer find the bug session” button is displayed on the right
    ...    6 Verify the download link for the flyer -> Link should be equal to https://qualityminds.de/app/uploads/2018/11/Find-The-Mobile-Bug-Session.pdf
    ...    7 Verify if file is available via download link -> File can be downloaded
    [Setup]    Create temp Download Folder
    Step 2: Hover on Portfolio bar and Check submenu
    Step 3: Click WAM Testing sub option and Check header and URL
    Step 4: Verify that the Portfolio item is highlighted
    Step 5: Click Mobile tab and Check section
    Step 5a: Check if Mobile section tab is underlined and grey
    Step 5b: Check if Flyer button is displayed on the right
    Step 6: Verify if Flyer link is what expected
    Step 7: Download Flyer and Check if it has size
    [Teardown]    Delete Folder    ${temp_DownloadFolder}
